# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as powerlevel10k with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('powerlevel10k-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_powerlevel10k', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

powerlevel10k-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

powerlevel10k-config-file-config-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/powerlevel10k
    - user: {{ name }}
    - group: {{ user_group }}
    - makedirs: True
    - mode: '0755'
    - require:
      - powerlevel10k-config-file-user-{{ name }}-present

powerlevel10k-config-file-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/powerlevel10k/powerlevel10k.zsh
    - source: {{ files_switch([
                   name ~ '-powerlevel10k.tmpl', 'powerlevel10k.tmpl'],
                 lookup='powerlevel10k-config-file-config-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - powerlevel10k-config-file-config-dir-{{ name }}-managed

powerlevel10k-config-file-zsh-init-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/init.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True

powerlevel10k-config-file-zsh-after-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/after.d
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True

powerlevel10k-config-file-init-file-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/init.d/01-powerlevel10k.zsh
    - source: {{ files_switch([
                   'init.d/' ~ name ~ '01-powerlevel10k.tmpl',
                   'init.d/01-powerlevel10k.tmpl'],
                 lookup='powerlevel10k-config-file-init-file-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - powerlevel10k-config-file-zsh-init-dir-{{ name }}-managed

powerlevel10k-config-file-after-file-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/after.d/99-powerlevel10k.zsh
    - source: {{ files_switch([
                   'after.d/' ~ name ~ '99-powerlevel10k.tmpl',
                   'after.d/99-powerlevel10k.tmpl'],
                 lookup='powerlevel10k-config-file-after-file-' ~ name ~ '-managed'
                 )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - require:
      - powerlevel10k-config-file-zsh-after-dir-{{ name }}-managed

{% endif %}
{% endfor %}
{% endif %}
