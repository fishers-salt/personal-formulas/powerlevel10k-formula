# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as powerlevel10k with context %}

{% if salt['pillar.get']('powerlevel10k-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_powerlevel10k', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

powerlevel10k-config-file-after-file-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/after.d/01-powerlevel10k.zsh

powerlevel10k-config-file-init-file-{{ name }}-absent:
  file.absent:
    - name: {{ home }}/.config/zsh/init.d/01-powerlevel10k.zsh

powerlevel10k-config-file-config-{{ name }}-managed:
  file.absent:
    - name: {{ home }}/.config/powerlevel10k/powerlevel10k.zsh

powerlevel10k-config-file-config-dir-{{ name }}-managed:
  file.absent:
    - name: {{ home }}/.config/powerlevel10k

{% endif %}
{% endfor %}
{% endif %}
