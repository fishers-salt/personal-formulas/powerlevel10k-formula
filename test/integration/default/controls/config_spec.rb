# frozen_string_literal: true

control 'powerlevel10k-config-file-config-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/powerlevel10k') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'powerlevel10k-config-file-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/powerlevel10k/powerlevel10k.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Temporarily change options.') }
  end
end

control 'powerlevel10k-config-file-zsh-init-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/init.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'powerlevel10k-config-file-zsh-after-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/after.d') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'powerlevel10k-config-file-init-file-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/init.d/01-powerlevel10k.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# Enable Powerlevel10k instant prompt.') }
    its('content') { should include('# Initialization code that may require') }
  end
end

control 'powerlevel10k-config-file-after-file-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/after.d/99-powerlevel10k.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('# To customize prompt, run `p10k') }
    its('content') { should include('source ~/.config/powerlevel10k') }
  end
end
