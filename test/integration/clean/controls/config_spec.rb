# frozen_string_literal: true

control 'powerlevel10k-config-file-init-file-auser-managed' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/init.d/01-powerlevel10k.zsh') do
    it { should_not exist }
  end
end

control 'powerlevel10k-config-file-after-file-auser-managed' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/after.d/99-powerlevel10k.zsh') do
    it { should_not exist }
  end
end

control 'powerlevel10k-config-file-config-auser-absent' do
  title 'should not exist'

  describe directory('/home/auser/.config/powerlevel10k') do
    it { should_not exist }
  end
end
control 'powerlevel10k-config-file-dir-auser-managed' do
  title 'should be absent'

  describe file('/home/auser/.config/powerlevel10k/powerlevel10k.zsh') do
    it { should_not exist }
  end
end
